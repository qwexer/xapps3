# -*- coding: utf-8 -*-
# Part of Axiom. See LICENSE file for full copyright and licensing details.

{
    'name': 'Web Routing',
    'author': 'Axiom',
    'summary': 'Web Routing',
    'sequence': '9100',
    'category': 'Hidden',
    'description': """
Proposes advanced routing options not available in web or base to keep
base modules simple.
""",
    'data': [
    ],
    'depends': ['web'],
}
