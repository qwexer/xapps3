Axiom Apps
===========

Axiom Apps is a suite of web based business applications. 

The main Axiom Apps include a CRM,Website Builder,eCommerce,Warehouse Management,
Project Management,Billing; Accounting,Point of Sale,Human Resources,Marketing,Manufacturing,
Purchase Management and many more.

Axiom Apps can be used as stand-alone applications, but they also integrate seamlessly so you get
a full-featured ERP when you install several Apps.

For Additional Apps for Axiom Apps, please contact info@axiomworld.net

Installation Instructions
-------------------------
For a standard installation please follow the instructions here: 

Copy Data:
	
	Copy and paste given folder AxiomApps to C: directroy.	

Python:

	Download python 3.5.4 https://www.python.org/downloads/windows/

	Create new folder in ‘C’ directory with name e.g: python3

	Install python in above folder using custom install option.

	Set it’s path and make sure that it is running through command line.

	Goto C:/python3/Scripts 

	Upgrade pip version, Run command: ‘pip install --upgrade pip’

	Install psycopg using the installer here http://www.stickpeople.com/projects/python/win-psycopg/

	Run C:\YourAxiomAppsPath> C:\Python35\Scripts\pip.exe install -r requirements.txt 
	It will install all python required packages.

Postgresql:
 
	Install postgresql 9.5 https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
 
	add PostgreSQL's bin directory (default: C:\Program Files\PostgreSQL\9.4\bin) to your PATH

Create User:
 
	Goto command line.
 
	Enter command psql -U postgres.
 
	Execute query CREATE USER axiom_user with PASSWORD 'axiom' CREATEDB SUPERUSER LOGIN ;
	You will see role created successfully message after executing above command.

Install Node:

	Download node https://nodejs.org/en/download/
	
	Install node and set it’s path.
	
	Run command: npm install -g less

All Done!

Run The Package: 
	
	Open command line. 
	Goto C:/AxiomApps.
	Run this Command: python axiom-bin --config=axiom-run.conf

	Open this Url http://localhost:8069/web  in your browser. 

	You will see a web page 
	Enter Details and create database. 
	
	Login using the credentials you've supplied while creating the database. 
	
	Goto to Apps.
	Install Invoice Management App. 
	